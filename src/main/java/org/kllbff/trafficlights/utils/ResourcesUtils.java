package org.kllbff.trafficlights.utils;

import android.content.res.Resources;

import androidx.core.content.res.ResourcesCompat;

public class ResourcesUtils {
    private static Resources res;

    public static void register(Resources res) {
        ResourcesUtils.res = res;
    }

    public static void unregister() {
        ResourcesUtils.res = null;
    }

    public static int getColor(int resId) {
        return ResourcesCompat.getColor(res, resId, null);
    }
}

package org.kllbff.trafficlights;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import org.kllbff.trafficlights.blinks.LightsBlinks;
import org.kllbff.trafficlights.utils.ResourcesUtils;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String KEY_WAS_BLINKED = "is-lights-was-blinked";
    private static final String KEY_COLOR = "lights-color";

    private View lightsView;
    private LightsBlinks blinks;
    private boolean wasBlinked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lightsView = findViewById(R.id.lights);
        findViewById(R.id.button_first).setOnClickListener(this);
        findViewById(R.id.button_second).setOnClickListener(this);
        findViewById(R.id.button_third).setOnClickListener(this);
        findViewById(R.id.button_shutdown).setOnClickListener(this);

        ResourcesUtils.register(getResources());
        if (blinks == null) {
            blinks = new LightsBlinks(new Handler(),
                    ResourcesUtils.getColor(R.color.lights_signals_shutdown),
                    ResourcesUtils.getColor(R.color.lights_signals_default));
        }

        if (savedInstanceState != null) {
            wasBlinked = savedInstanceState.getBoolean(KEY_WAS_BLINKED);
            if (!wasBlinked) {
                int color = savedInstanceState.getInt(KEY_COLOR);
                lightsView.setBackgroundColor(color);
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        ResourcesUtils.register(getResources());
        if(wasBlinked) {
            blinks.start(lightsView);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        ResourcesUtils.unregister();
    }

    @Override
    public void onClick(View view) {
        switch(view.getId()) {
            case R.id.button_first:
                blinks.stop();
                setLightsViewColor(R.color.lights_signals_first);
            break;
            case R.id.button_second:
                blinks.stop();
                setLightsViewColor(R.color.lights_signals_second);
            break;
            case R.id.button_third:
                blinks.stop();
                setLightsViewColor(R.color.lights_signals_third);
            break;
            case R.id.button_shutdown:
                if(!wasBlinked) {
                    blinks.start(lightsView);
                }
            break;
        }
    }

    private void setLightsViewColor(int resId) {
        lightsView.setBackgroundColor(ResourcesUtils.getColor(resId));
    }
}

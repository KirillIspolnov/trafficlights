package org.kllbff.trafficlights.blinks;

import android.os.Handler;
import android.view.View;

public class LightsBlinks {
    private static final String TAG_NAME = LightsBlinks.class.getName();

    private View lightsView;
    private int blinkColor, defaultColor;
    private boolean wasBlinked;

    private Object locker;
    private Handler handler, uiHandler;

    public LightsBlinks(Handler uiHandler, int blinkColor, int defaultColor) {
        this.uiHandler = uiHandler;
        this.blinkColor = blinkColor;
        this.defaultColor = defaultColor;

        locker = "Hello man, I'm a locker";
        BlinkThread blinkThread = new BlinkThread(locker);
        blinkThread.start();
        handler = blinkThread.getHandler();
        this.wasBlinked = false;
    }

    public void start(View lightsView) {
        this.lightsView = lightsView;
        BlinkTask task = new BlinkTask();
        task.run();
    }

    public void stop() {
        this.lightsView = null;
    }

    private class BlinkTask implements Runnable {
        @Override
        public void run() {
            if(lightsView == null) {
                return;
            }

            uiHandler.post(() -> {
                if(wasBlinked) {
                    lightsView.setBackgroundColor(blinkColor);
                } else {
                    lightsView.setBackgroundColor(defaultColor);
                }
            });

            wasBlinked = !wasBlinked;
            handler.postDelayed(new BlinkTask(), 1000L);
        }
    }
}

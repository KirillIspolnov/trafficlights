package org.kllbff.trafficlights.blinks;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

public class BlinkThread extends Thread {
    private static final String TAG_NAME = BlinkThread.class.getName();

    private Object locker;
    private Handler handler;

    public BlinkThread(Object locker) {
        this.locker = locker;
    }

    @Override
    public void run() {
        Log.i(TAG_NAME, "Preparing");
        Looper.prepare();
        handler = new Handler();
        Looper.loop();
    }

    public Handler getHandler() {
        while(handler == null) { }
        return handler;
    }

}
